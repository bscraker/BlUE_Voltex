﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlUE_Voltex_NoteEditor.Class
{
    class Data
    {
        public int[] knob = {0, 0};
        public int[] joystick = {0, 0, 0};
        public int cancel = 0;

        public Data(String data)
        {
            String[] datas = data.Split(new char[] {' '}, StringSplitOptions.None);

            if (datas.Length == 7)
            {
                int.TryParse(datas[0], out knob[0]);
                int.TryParse(datas[1], out knob[1]);
                int.TryParse(datas[2], out joystick[0]);
                int.TryParse(datas[3], out joystick[1]);
                int.TryParse(datas[4], out cancel);
                int.TryParse(datas[5], out joystick[2]);
            }
        }
    }
}

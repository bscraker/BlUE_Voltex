﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlUE_Voltex_NoteEditor.Class
{
    class Music
    {
        public const int MAX_DIFFICULTY = 3;
        public const int MAX_NOTES = 2;
        public const String FILE_PATH = ".\\music\\";
        public const String FILE_INFORMATION = "information.dat";
        public const String FILE_IMAGE = "image.png";
        public const String FILE_MUSIC = "music.mp3";
        public const String FILE_HIT = "hit.wav";
        public const String FILE_NOTE_EASY_L = "note_easy_l.dat";
        public const String FILE_NOTE_EASY_R = "note_easy_r.dat";
        public const String FILE_NOTE_EASY_SPEED = "note_easy_speed.dat";
        public const String FILE_NOTE_EASY_BPM = "note_easy_bpm.dat";
        public const String FILE_NOTE_NORMAL_L = "note_normal_l.dat";
        public const String FILE_NOTE_NORMAL_R = "note_normal_r.dat";
        public const String FILE_NOTE_NORMAL_SPEED = "note_normal_speed.dat";
        public const String FILE_NOTE_NORMAL_BPM = "note_normal_bpm.dat";
        public const String FILE_NOTE_HARD_L = "note_hard_l.dat";
        public const String FILE_NOTE_HARD_R = "note_hard_r.dat";
        public const String FILE_NOTE_HARD_SPEED = "note_hard_speed.dat";
        public const String FILE_NOTE_HARD_BPM = "note_hard_bpm.dat";

        public String[][] FILE_NOTE;
        public String[] FILE_SPEED;
        public String[] FILE_BPM;

        public String saveName = "";
        public String savePath = "";

        public Information information = null;
        public List<Note>[][] notes = null;
        public List<Speed>[] speeds = null;
        public List<BPM>[] bpms = null;

        public Music()
        {
            information = new Information();

            FILE_NOTE = new String[3][];
            FILE_NOTE[0] = new String[] { FILE_NOTE_EASY_L, FILE_NOTE_EASY_R };
            FILE_NOTE[1] = new String[] { FILE_NOTE_NORMAL_L, FILE_NOTE_NORMAL_R };
            FILE_NOTE[2] = new String[] { FILE_NOTE_HARD_L, FILE_NOTE_HARD_R };

            FILE_SPEED = new String[] { FILE_NOTE_EASY_SPEED, FILE_NOTE_NORMAL_SPEED, FILE_NOTE_HARD_SPEED };
            FILE_BPM = new String[] { FILE_NOTE_EASY_BPM, FILE_NOTE_NORMAL_BPM, FILE_NOTE_HARD_BPM };

            Initialize();
        }

        private void Initialize()
        {
            notes = new List<Note>[MAX_DIFFICULTY][];

            for (int i = 0; i < MAX_DIFFICULTY; ++i)
            {
                notes[i] = new List<Note>[MAX_NOTES];

                for (int j = 0; j < MAX_NOTES; ++j)
                {
                    notes[i][j] = new List<Note>();
                }
            }

            speeds = new List<Speed>[MAX_DIFFICULTY];

            for (int i = 0; i < MAX_DIFFICULTY; ++i)
            {
                speeds[i] = new List<Speed>();
            }

            bpms = new List<BPM>[MAX_DIFFICULTY];

            for (int i = 0; i < MAX_DIFFICULTY; ++i)
            {
                bpms[i] = new List<BPM>();
            }
        }

        public bool AddNote(int difficulty, int index, Note note)
        {
            if (note.beatNumerator >= note.beatDenominator ||
                note.positionNumerator > note.positionDenominator)
            {
                return false;
            }

            List<BPM> bpms_temp = bpms[difficulty];

            int bpmMAX = bpms_temp.Count;

            for (int j = 0; j < bpmMAX; ++j)
            {
                if (bpms_temp[j].order < note.order)
                {
                    continue;
                }
                else if (bpms_temp[j].order > note.order)
                {
                    break;
                }
                else
                {
                    int bpm_old = bpms_temp[j].beatNumerator * note.beatDenominator;
                    int bpm_new = note.beatNumerator * bpms_temp[j].beatDenominator;

                    if (bpm_old < bpm_new)
                    {
                        return false;
                    }
                    else if (bpm_old > bpm_new)
                    {
                        break;
                    }
                }
            }

            List<Note> notes_temp = notes[difficulty][index];

            bool result = true;
            int insert = 0;

            int MAX = notes_temp.Count;

            for (int i = 0; i < MAX; ++i)
            {
                if (notes_temp[i].order < note.order)
                {
                    insert = i + 1;
                    continue;
                }
                else if (notes_temp[i].order > note.order)
                {
                    break;
                }
                else
                {
                    int note_old = notes_temp[i].beatNumerator * note.beatDenominator;
                    int note_new = note.beatNumerator * notes_temp[i].beatDenominator;

                    if (note_old < note_new)
                    {
                        insert = i + 1;
                        continue;
                    }
                    else if (note_old > note_new)
                    {
                        break;
                    }
                    else
                    {
                        result = false;
                        break;
                    }
                }
            }

            if (result)
            {
                if (insert > 0 &&
                    notes_temp[insert - 1].longNote)
                {
                    result = false;
                }
                else
                {
                    notes[difficulty][index].Insert(insert, note);
                }
            }

            return result;
        }

        public bool AddLongNote(int difficulty, int index, Note note)
        {
            if (note.beatNumerator >= note.beatDenominator ||
                note.positionNumerator > note.positionDenominator)
            {
                return false;
            }

            bool result = false;

            int findIndex = -1;

            List<Note> notes_temp = notes[difficulty][index];
            int MAX = notes_temp.Count;

            for (int i = 0; i < MAX; ++i)
            {
                if (notes_temp[i].order < note.order)
                {
                    continue;
                }
                else if (notes_temp[i].order == note.order &&
                    notes_temp[i].beatNumerator == note.beatNumerator &&
                    notes_temp[i].beatDenominator == note.beatDenominator &&
                    notes_temp[i].positionNumerator == note.positionNumerator &&
                    notes_temp[i].positionDenominator == note.positionDenominator &&
                    notes_temp[i].longNote == note.longNote)
                {
                    result = true;
                    findIndex = i;
                    break;
                }
                else if (notes_temp[i].order > note.order)
                {
                    break;
                }
            }

            if (result)
            {
                if (notes_temp[findIndex].longNote)
                {
                    notes[difficulty][index][findIndex].longNote = false;
                }
                else if (findIndex < MAX - 1 &&
                    notes_temp[findIndex + 1].positionNumerator * note.positionDenominator ==
                    note.positionNumerator * notes_temp[findIndex + 1].positionDenominator)
                {
                    notes[difficulty][index][findIndex].longNote = true;
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        public bool AddSpeed(int difficulty, Speed speed)
        {
            List<BPM> bpms_temp = bpms[difficulty];

            int bpmMAX = bpms_temp.Count;

            for (int j = 0; j < bpmMAX; ++j)
            {
                if (bpms_temp[j].order < speed.order)
                {
                    continue;
                }
                else if (bpms_temp[j].order > speed.order)
                {
                    break;
                }
                else
                {
                    int bpm_old = bpms_temp[j].beatNumerator * speed.beatDenominator;
                    int bpm_new = speed.beatNumerator * bpms_temp[j].beatDenominator;

                    if (bpm_old < bpm_new)
                    {
                        return false;
                    }
                    else if (bpm_old > bpm_new)
                    {
                        break;
                    }
                }
            }

            List<Speed> speeds_temp = speeds[difficulty];

            bool result = true;
            int insert = 0;

            int speedMAX = speeds_temp.Count;

            for (int i = 0; i < speedMAX; ++i)
            {
                if (speeds_temp[i].order < speed.order)
                {
                    insert = i + 1;
                    continue;
                }
                else if (speeds_temp[i].order > speed.order)
                {
                    break;
                }
                else
                {
                    int speed_old = speeds_temp[i].beatNumerator * speed.beatDenominator;
                    int speed_new = speed.beatNumerator * speeds_temp[i].beatDenominator;

                    if (speed_old < speed_new)
                    {
                        insert = i + 1;
                        continue;
                    }
                    else if (speed_old > speed_new)
                    {
                        break;
                    }
                    else
                    {
                        result = false;
                        break;
                    }
                }
            }

            if (result)
            {
                speeds[difficulty].Insert(insert, speed);
            }

            return result;
        }

        public bool AddBPM(int difficulty, BPM bpm)
        {
            for (int i = 0; i < MAX_NOTES; ++i)
            {
                List<Note> notes_temp = notes[difficulty][i];

                int noteMAX = notes_temp.Count;

                for (int j = 0; j < noteMAX; ++j)
                {
                    if (notes_temp[j].order < bpm.order)
                    {
                        continue;
                    }
                    else if (notes_temp[j].order > bpm.order)
                    {
                        break;
                    }
                    else
                    {
                        int note_old = notes_temp[j].beatNumerator * bpm.beatDenominator;
                        int note_new = bpm.beatNumerator * notes_temp[j].beatDenominator;

                        if (note_old < note_new)
                        {
                            continue;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }

            List<Speed> speeds_temp = speeds[difficulty];

            int speedMAX = speeds_temp.Count;

            for (int j = 0; j < speedMAX; ++j)
            {
                if (speeds_temp[j].order < bpm.order)
                {
                    continue;
                }
                else if (speeds_temp[j].order > bpm.order)
                {
                    break;
                }
                else
                {
                    int speed_old = speeds_temp[j].beatNumerator * bpm.beatDenominator;
                    int speed_new = bpm.beatNumerator * speeds_temp[j].beatDenominator;

                    if (speed_old < speed_new)
                    {
                        continue;
                    }
                    else if (speed_old > speed_new)
                    {
                        break;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            List<BPM> bpms_temp = bpms[difficulty];

            bool result = true;
            int insert = 0;

            int bpmMAX = bpms_temp.Count;

            for (int j = 0; j < bpmMAX; ++j)
            {
                if (bpms_temp[j].order < bpm.order)
                {
                    insert = j + 1;
                    continue;
                }
                else if (bpms_temp[j].order > bpm.order)
                {
                    break;
                }
                else
                {
                    result = false;
                    break;
                }
            }

            if (result)
            {
                bpms[difficulty].Insert(insert, bpm);
            }

            return result;
        }

        public bool AddDragNote(int difficulty, int index, Note note, int beatDenominator, int type)
        {
            if (note.beatNumerator >= note.beatDenominator ||
                note.positionNumerator > note.positionDenominator)
            {
                return false;
            }

            bool result = false;

            int findIndex = -1;

            List<Note> notes_temp = notes[difficulty][index];
            int MAX = notes_temp.Count;

            for (int i = 0; i < MAX; ++i)
            {
                if (notes_temp[i].order < note.order)
                {
                    continue;
                }
                else if (notes_temp[i].order == note.order &&
                    notes_temp[i].beatNumerator == note.beatNumerator &&
                    notes_temp[i].beatDenominator == note.beatDenominator &&
                    notes_temp[i].positionNumerator == note.positionNumerator &&
                    notes_temp[i].positionDenominator == note.positionDenominator &&
                    notes_temp[i].longNote == note.longNote &&
                    i < MAX - 1)
                {
                    result = true;
                    findIndex = i;
                    break;
                }
                else if (notes_temp[i].order > note.order)
                {
                    break;
                }
            }

            if (result)
            {
                Note endNote = notes_temp[findIndex + 1];

                Note tempEndNote = new Note();
                tempEndNote.order = endNote.order;
                tempEndNote.beatNumerator = endNote.beatNumerator;
                tempEndNote.beatDenominator = endNote.beatDenominator;
                tempEndNote.positionNumerator = endNote.positionNumerator;
                tempEndNote.positionDenominator = endNote.positionDenominator;
                tempEndNote.longNote = endNote.longNote;

                if (!note.longNote &&
                    (note.beatNumerator * beatDenominator) % note.beatDenominator == 0 &&
                    (tempEndNote.beatNumerator * beatDenominator) % tempEndNote.beatDenominator == 0)
                {
                    int insertIndex = findIndex;

                    Note tempNote = new Note();
                    tempNote.order = note.order;
                    tempNote.beatNumerator = note.beatNumerator;
                    tempNote.beatDenominator = note.beatDenominator;
                    tempNote.positionNumerator = note.positionNumerator;
                    tempNote.positionDenominator = note.positionDenominator;
                    tempNote.longNote = note.longNote;

                    int beatDenominatorCount = tempNote.beatDenominator * tempEndNote.beatDenominator * beatDenominator;
                    /*int positionDenominatorCount = tempNote.positionDenominator * tempEndNote.positionDenominator *
                                                   beatDenominator;*/

                    tempNote.beatNumerator *= beatDenominatorCount / tempNote.beatDenominator;
                    tempNote.beatDenominator *= beatDenominatorCount / tempNote.beatDenominator;

                    tempEndNote.beatNumerator *= beatDenominatorCount / tempEndNote.beatDenominator;
                    tempEndNote.beatDenominator *= beatDenominatorCount / tempEndNote.beatDenominator;

                    int tempNoteBeat = tempNote.beatNumerator * tempEndNote.beatDenominator * beatDenominator;
                    int tempEndNoteBeat = tempEndNote.beatNumerator * tempNote.beatDenominator * beatDenominator;

                    int denominatorCount = (tempNoteBeat - tempEndNoteBeat);
                    denominatorCount /= (tempNote.beatDenominator * tempEndNote.beatDenominator);
                    denominatorCount = Math.Abs(denominatorCount);

                    tempNote.positionNumerator *= denominatorCount * tempEndNote.positionDenominator;
                    tempNote.positionDenominator *= denominatorCount * tempEndNote.positionDenominator;

                    tempEndNote.positionNumerator *= denominatorCount * note.positionDenominator;
                    tempEndNote.positionDenominator *= denominatorCount * note.positionDenominator;

                    int startPositionNumerator = tempNote.positionNumerator;

                    while (insertIndex - findIndex < denominatorCount - 1)
                    {
                        tempNote.beatNumerator += beatDenominatorCount / beatDenominator;

                        if (tempNote.beatNumerator >= tempNote.beatDenominator)
                        {
                            ++tempNote.order;
                            tempNote.beatNumerator -= tempNote.beatDenominator;
                        }

                        if (type == 0)
                        {
                            tempNote.positionNumerator += (tempEndNote.positionNumerator - startPositionNumerator) /
                                                          denominatorCount;
                        }
                        else if (type == 1)
                        {
                            double startPosition = (double)startPositionNumerator / tempEndNote.positionDenominator;
                            double movePostion = (double)(insertIndex - findIndex + 1) / denominatorCount;
                            double distance = (double)(tempEndNote.positionNumerator - startPositionNumerator) /
                                              tempEndNote.positionDenominator;
                            startPosition += (Math.Sin(movePostion * Math.PI / 2)) * distance;

                            tempNote.positionNumerator = (int)(startPosition * 1023);
                            tempNote.positionDenominator = 1023;
                        }
                        else if (type == 2)
                        {
                            double startPosition = (double)startPositionNumerator / tempEndNote.positionDenominator;
                            double movePostion = (double)(insertIndex - findIndex + 1) / denominatorCount;
                            double distance = (double)(tempEndNote.positionNumerator - startPositionNumerator) /
                                              tempEndNote.positionDenominator;
                            startPosition += (1 - Math.Sin((movePostion + 1.0) * Math.PI / 2)) * distance;

                            tempNote.positionNumerator = (int)(startPosition * 1023);
                            tempNote.positionDenominator = 1023;
                        }

                        ++insertIndex;

                        if (insertIndex > findIndex)
                        {
                            Note newNote = new Note();
                            newNote.order = tempNote.order;
                            newNote.beatNumerator = tempNote.beatNumerator;
                            newNote.beatDenominator = tempNote.beatDenominator;
                            newNote.positionNumerator = tempNote.positionNumerator;
                            newNote.positionDenominator = tempNote.positionDenominator;
                            newNote.longNote = tempNote.longNote;

                            AddNote(difficulty, index, newNote);
                        }
                    }
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        public bool DeleteNote(int difficulty, int index, Note note)
        {
            bool result = false;

            int findIndex = -1;

            List<Note> notes_temp = notes[difficulty][index];
            int MAX = notes_temp.Count;

            for (int i = 0; i < MAX; ++i)
            {
                if (notes_temp[i].order < note.order)
                {
                    continue;
                }
                else if (notes_temp[i].order == note.order &&
                    notes_temp[i].beatNumerator == note.beatNumerator &&
                    notes_temp[i].beatDenominator == note.beatDenominator &&
                    notes_temp[i].positionNumerator == note.positionNumerator &&
                    notes_temp[i].positionDenominator == note.positionDenominator &&
                    notes_temp[i].longNote == note.longNote)
                {
                    result = true;
                    findIndex = i;
                    break;
                }
                else if (notes_temp[i].order > note.order)
                {
                    break;
                }
            }

            if (result)
            {
                if (findIndex > 0 &&
                    notes_temp[findIndex - 1].longNote)
                {
                    notes[difficulty][index][findIndex - 1].longNote = false;
                }

                notes[difficulty][index].RemoveAt(findIndex);
            }

            return result;
        }

        public bool DeleteSpeed(int difficulty, Speed speed)
        {
            bool result = false;

            int findIndex = -1;

            int MAX = speeds[difficulty].Count;

            for (int i = 0; i < MAX; ++i)
            {
                if (speeds[difficulty][i].order < speed.order)
                {
                    continue;
                }
                else if (speeds[difficulty][i].order == speed.order &&
                    speeds[difficulty][i].beatNumerator == speed.beatNumerator &&
                    speeds[difficulty][i].beatDenominator == speed.beatDenominator &&
                    speeds[difficulty][i].speed == speed.speed)
                {
                    result = true;
                    findIndex = i;
                    break;
                }
                else if (speeds[difficulty][i].order > speed.order)
                {
                    break;
                }
            }

            if (result)
            {
                speeds[difficulty].RemoveAt(findIndex);
            }

            return result;
        }

        public bool DeleteBPM(int difficulty, BPM bpm)
        {
            bool result = false;

            int findIndex = -1;

            int MAX = bpms[difficulty].Count;

            for (int i = 0; i < MAX; ++i)
            {
                if (bpms[difficulty][i].order < bpm.order)
                {
                    continue;
                }
                else if (bpms[difficulty][i].order == bpm.order &&
                    bpms[difficulty][i].beatNumerator == bpm.beatNumerator &&
                    bpms[difficulty][i].beatDenominator == bpm.beatDenominator &&
                    bpms[difficulty][i].bpm == bpm.bpm)
                {
                    result = true;
                    findIndex = i;
                    break;
                }
                else if (bpms[difficulty][i].order > bpm.order)
                {
                    break;
                }
            }

            if (result)
            {
                bpms[difficulty].RemoveAt(findIndex);
            }

            return result;
        }

        public void Sort()
        {
            for (int i = 0; i < MAX_DIFFICULTY; ++i)
            {
                for (int j = 0; j < MAX_NOTES; ++j)
                {
                    notes[i][j].Sort(
                        delegate(Note note1, Note note2)
                        {
                            int result = note1.order.CompareTo(note2.order);

                            if (result == 0)
                            {
                                result = (note1.beatNumerator * note2.beatDenominator)
                                         .CompareTo(note2.beatNumerator * note1.beatDenominator);
                            }

                            return result;
                        });
                }
            }

            for (int i = 0; i < MAX_DIFFICULTY; ++i)
            {
                speeds[i].Sort(
                        delegate(Speed speed1, Speed speed2)
                        {
                            int result = speed1.order.CompareTo(speed2.order);

                            if (result == 0)
                            {
                                result = (speed1.beatNumerator * speed2.beatDenominator)
                                         .CompareTo(speed2.beatNumerator * speed1.beatDenominator);
                            }

                            return result;
                        });
            }

            for (int i = 0; i < MAX_DIFFICULTY; ++i)
            {
                bpms[i].Sort(
                        delegate(BPM bpm1, BPM bpm2)
                        {
                            int result = bpm1.order.CompareTo(bpm2.order);

                            if (result == 0)
                            {
                                result = (bpm1.beatNumerator * bpm2.beatDenominator)
                                         .CompareTo(bpm2.beatNumerator * bpm1.beatDenominator);
                            }

                            return result;
                        });
            }
        }

        public bool Create(String _saveName, String imageFile, String musicFile, String hitFile)
        {
            if (!Directory.Exists(FILE_PATH))
            {
                Directory.CreateDirectory(FILE_PATH);
            }

            if (Directory.Exists(FILE_PATH + _saveName))
            {
                return false;
            }

            saveName = _saveName;
            savePath = FILE_PATH + _saveName + "\\";

            Directory.CreateDirectory(savePath);

            File.Copy(imageFile, savePath + "\\" + FILE_IMAGE);
            File.Copy(musicFile, savePath + "\\" + FILE_MUSIC);
            File.Copy(hitFile, savePath + "\\" + FILE_HIT);

            return true;
        }

        public void SaveInformation(String name, String artist, String BPM, String startTime,
                                    String previewStartTime, String previewPlayTime,
                                    String musicVolume, String hitVolume,
                                    String hardDifficulty, String normalDifficulty, String easyDifficulty)
        {
            String information_text = name + "|" +
                                      artist + "|" +
                                      BPM + "|" +
                                      startTime + "|" +
                                      previewStartTime + "|" +
                                      previewPlayTime + "|" +
                                      musicVolume + "|" +
                                      hitVolume + "|" +
                                      hardDifficulty + "|" +
                                      normalDifficulty + "|" +
                                      easyDifficulty;

            information.name = name;
            information.artist = artist;

            float.TryParse(BPM, out information.startBPM);
            int.TryParse(startTime, out information.startTime);
            int.TryParse(previewStartTime, out information.previewStartTime);
            int.TryParse(previewPlayTime, out information.previewPlayTime);
            float.TryParse(musicVolume, out information.musicVolume);
            float.TryParse(hitVolume, out information.hitVolume);
            int.TryParse(hardDifficulty, out information.hardDifficulty);
            int.TryParse(normalDifficulty, out information.normalDifficulty);
            int.TryParse(easyDifficulty, out information.easyDifficulty);

            WriteText(savePath + FILE_INFORMATION, information_text);
        }

        public void SaveNote()
        {
            Sort();

            for (int i = 0; i < MAX_DIFFICULTY; ++i)
            {
                for (int j = 0; j < MAX_NOTES; ++j)
                {
                    String note_text = "";

                    int MAX = notes[i][j].Count;

                    for (int k = 0; k < MAX; ++k)
                    {
                        if (k > 0)
                        {
                            note_text += "\n";
                        }

                        String longNote = "0";

                        if (notes[i][j][k].longNote)
                        {
                            longNote = "1";
                        }

                        note_text += notes[i][j][k].order.ToString() + "|" +
                                     notes[i][j][k].beatNumerator.ToString() + "|" +
                                     notes[i][j][k].beatDenominator.ToString() + "|" +
                                     notes[i][j][k].positionNumerator.ToString() + "|" +
                                     notes[i][j][k].positionDenominator.ToString() + "|" +
                                     longNote;
                    }

                    WriteText(savePath + FILE_NOTE[i][j], note_text);
                }
            }

            for (int i = 0; i < MAX_DIFFICULTY; ++i)
            {
                String speed_text = "";

                int MAX = speeds[i].Count;

                for (int j = 0; j < MAX; ++j)
                {
                    if (j > 0)
                    {
                        speed_text += "\n";
                    }

                    speed_text += speeds[i][j].order.ToString() + "|" +
                                  speeds[i][j].beatNumerator.ToString() + "|" +
                                  speeds[i][j].beatDenominator.ToString() + "|" +
                                  speeds[i][j].speed.ToString();
                }

                WriteText(savePath + FILE_SPEED[i], speed_text);
            }

            for (int i = 0; i < MAX_DIFFICULTY; ++i)
            {
                String bpm_text = "";

                int MAX = bpms[i].Count;

                for (int j = 0; j < MAX; ++j)
                {
                    if (j > 0)
                    {
                        bpm_text += "\n";
                    }

                    bpm_text += bpms[i][j].order.ToString() + "|" +
                                bpms[i][j].beatNumerator.ToString() + "|" +
                                bpms[i][j].beatDenominator.ToString() + "|" +
                                bpms[i][j].bpm.ToString();
                }

                WriteText(savePath + FILE_BPM[i], bpm_text);
            }
        }

        public int Load(String path, bool onlyInformation = false)
        {
            if (!path.EndsWith("\\"))
            {
                path += "\\";
            }

            if (!File.Exists(path + FILE_IMAGE) || !File.Exists(path + FILE_MUSIC))
            {
                return 0;
            }

            savePath = path;

            Initialize();

            if (!File.Exists(path + FILE_INFORMATION))
            {
                return 1;
            }

            String information_text = ReadText(savePath + FILE_INFORMATION);
            String[] informations = information_text.Split(new String[] { "|" }, StringSplitOptions.None);

            if (informations.Length < 7)
            {
                return 1;
            }

            information.name = informations[0];
            information.artist = informations[1];

            if (!File.Exists(path + FILE_NOTE_EASY_L) || !File.Exists(path + FILE_NOTE_EASY_R) ||
                !File.Exists(path + FILE_NOTE_NORMAL_L) || !File.Exists(path + FILE_NOTE_NORMAL_R) ||
                !File.Exists(path + FILE_NOTE_HARD_L) || !File.Exists(path + FILE_NOTE_HARD_R) ||
                !File.Exists(path + FILE_NOTE_EASY_SPEED) ||
                !File.Exists(path + FILE_NOTE_NORMAL_SPEED) ||
                !File.Exists(path + FILE_NOTE_HARD_SPEED) ||
                !File.Exists(path + FILE_NOTE_EASY_BPM) ||
                !File.Exists(path + FILE_NOTE_NORMAL_BPM) ||
                !File.Exists(path + FILE_NOTE_HARD_BPM))
            {
                return 1;
            }

            float.TryParse(informations[2], out information.startBPM);
            int.TryParse(informations[3], out information.startTime);
            int.TryParse(informations[4], out information.previewStartTime);
            int.TryParse(informations[5], out information.previewPlayTime);
            float.TryParse(informations[6], out information.musicVolume);
            float.TryParse(informations[7], out information.hitVolume);
            int.TryParse(informations[8], out information.hardDifficulty);
            int.TryParse(informations[9], out information.normalDifficulty);
            int.TryParse(informations[10], out information.easyDifficulty);

            if (onlyInformation)
            {
                return 1;
            }

            for (int i = 0; i < MAX_DIFFICULTY; ++i)
            {
                for (int j = 0; j < MAX_NOTES; ++j)
                {
                    String note_text = ReadText(savePath + FILE_NOTE[i][j]);
                    String[] notes_text = note_text.Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

                    int MAX = notes_text.Length;

                    for (int k = 0; k < MAX; ++k)
                    {
                        String[] notes_values = notes_text[k].Split(new String[] { "|" }, StringSplitOptions.None);

                        int longNote = 0;

                        Note note = new Note();
                        int.TryParse(notes_values[0], out note.order);
                        int.TryParse(notes_values[1], out note.beatNumerator);
                        int.TryParse(notes_values[2], out note.beatDenominator);
                        int.TryParse(notes_values[3], out note.positionNumerator);
                        int.TryParse(notes_values[4], out note.positionDenominator);
                        int.TryParse(notes_values[5], out longNote);

                        if (longNote == 1)
                        {
                            note.longNote = true;
                        }

                        notes[i][j].Add(note);
                    }
                }
            }

            for (int i = 0; i < MAX_DIFFICULTY; ++i)
            {
                String speed_text = ReadText(savePath + FILE_SPEED[i]);
                String[] speeds_text = speed_text.Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

                int MAX = speeds_text.Length;

                for (int j = 0; j < MAX; ++j)
                {
                    String[] speeds_values = speeds_text[j].Split(new String[] { "|" }, StringSplitOptions.None);

                    Speed speed = new Speed();
                    int.TryParse(speeds_values[0], out speed.order);
                    int.TryParse(speeds_values[1], out speed.beatNumerator);
                    int.TryParse(speeds_values[2], out speed.beatDenominator);
                    float.TryParse(speeds_values[3], out speed.speed);

                    speeds[i].Add(speed);
                }
            }

            for (int i = 0; i < MAX_DIFFICULTY; ++i)
            {
                String bpm_text = ReadText(savePath + FILE_BPM[i]);
                String[] bpms_text = bpm_text.Split(new String[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);

                int MAX = bpms_text.Length;

                for (int j = 0; j < MAX; ++j)
                {
                    String[] bpms_values = bpms_text[j].Split(new String[] { "|" }, StringSplitOptions.None);

                    BPM bpm = new BPM();
                    int.TryParse(bpms_values[0], out bpm.order);
                    int.TryParse(bpms_values[1], out bpm.beatNumerator);
                    int.TryParse(bpms_values[2], out bpm.beatDenominator);
                    float.TryParse(bpms_values[3], out bpm.bpm);

                    bpms[i].Add(bpm);
                }
            }

            return 2;
        }

        public String ReadText(String path)
        {
            String text = "";

            if (File.Exists(path))
            {
                StreamReader streamReader = new StreamReader(path);
                text = streamReader.ReadToEnd();
                streamReader.Close();
                streamReader.Dispose();
            }

            return text;
        }

        public void WriteText(String path, String text)
        {
            StreamWriter streamWriter = new StreamWriter(path);

            streamWriter.Write(text);
            streamWriter.Close();
            streamWriter.Dispose();
        }
    }
}

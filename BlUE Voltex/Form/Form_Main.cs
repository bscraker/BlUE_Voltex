﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;
using System.IO;
using System.Management;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace BlUE_Voltex
{
    using BlUE_Voltex_NoteEditor.Class;
    using ControlCustom;
    using FMOD;

    public partial class Form_Main : Form
    {
        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);


        PrivateFontCollection privateFont;

        Font font_musicName;
        Font font_musicInformation;
        Font font_difficulty;
        Font font_speed;

        List<String> musicList;

        PictureBox[] albumList;
        PictureBox[] difficultyList;

        int state;
        int currentPage;
        int selectedMusicTick;
        int selectedMusicIndex;
        int selectedDifficultyIndex;
        int speed;

        String datas;
        Data data;

        int moveTick;
        bool isCancelled;
        bool isClicked;

        Music music;

        System system;
        Channel channel_music;
        Channel channel_ok;
        Channel channel_cancel;
        Channel channel_select;
        Sound sound_music;
        Sound sound_ok;
        Sound sound_cancel;
        Sound sound_select;

        Bitmap albumSmallBitmap;
        Bitmap albumBigBitmap;

        PictureLabel pictureLabel_Music_Name;
        PictureLabel pictureLabel_Music_Information_Label;
        PictureLabel pictureLabel_Music_Information;
        PictureLabel pictureLabel_Difficulty_Hard;
        PictureLabel pictureLabel_Difficulty_Hard_Score;
        PictureLabel pictureLabel_Difficulty_Normal;
        PictureLabel pictureLabel_Difficulty_Normal_Score;
        PictureLabel pictureLabel_Difficulty_Easy;
        PictureLabel pictureLabel_Difficulty_Easy_Score;
        PictureLabel pictureLabel_Speed;

        List<Control>[] controls;

        int playState;

        public Form_Main()
        {
            InitializeComponent();

            base.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            base.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            base.SetStyle(ControlStyles.ResizeRedraw, true);

            FormBorderStyle = FormBorderStyle.FixedSingle;

            Left = (Screen.PrimaryScreen.Bounds.Width - Width) / 2;
            Top = (Screen.PrimaryScreen.Bounds.Height - Height) / 2;

            privateFont = new PrivateFontCollection();
            privateFont.AddFontFile("font/Neues Bauen.ttf");

            font_musicName = new Font(privateFont.Families[0], 30.0f);
            font_musicInformation = new Font(privateFont.Families[0], 24.0f);
            font_difficulty = new Font(privateFont.Families[0], 24.0f);
            font_speed = new Font(privateFont.Families[0], 36.0f);

            musicList = GetFileList("music");

            albumList = new PictureBox[8];
            albumList[0] = pictureBox_AlbumList1;
            albumList[1] = pictureBox_AlbumList2;
            albumList[2] = pictureBox_AlbumList3;
            albumList[3] = pictureBox_AlbumList4;
            albumList[4] = pictureBox_AlbumList5;
            albumList[5] = pictureBox_AlbumList6;
            albumList[6] = pictureBox_AlbumList7;
            albumList[7] = pictureBox_AlbumList8;

            difficultyList = new PictureBox[4];
            difficultyList[0] = pictureBox_Difficulty_Hard;
            difficultyList[1] = pictureBox_Difficulty_Normal;
            difficultyList[2] = pictureBox_Difficulty_Easy;
            difficultyList[3] = pictureBox_Difficulty_Cancel;

            state = 0;
            currentPage = 0;
            selectedMusicTick = 0;
            selectedMusicIndex = 0;
            selectedDifficultyIndex = 0;
            speed = 10;

            datas = "";
            data = null;

            moveTick = 0;
            isCancelled = false;
            isClicked = false;

            music = new Music();

            Factory.System_Create(out system);
            system.init(32, INITFLAGS.NORMAL, (IntPtr)null);

            system.createSound("sound/ok.mp3", MODE.DEFAULT, out sound_ok);
            system.createSound("sound/cancel.mp3", MODE.DEFAULT, out sound_cancel);
            system.createSound("sound/select.mp3", MODE.DEFAULT, out sound_select);

            albumSmallBitmap = new Bitmap(albumList[0].Width, albumList[0].Height);
            albumBigBitmap = new Bitmap(pictureBox_Album_Selected.Width, pictureBox_Album_Selected.Height);

            pictureLabel_Music_Name = new PictureLabel();
            pictureLabel_Music_Information_Label = new PictureLabel();
            pictureLabel_Music_Information = new PictureLabel();
            pictureLabel_Difficulty_Hard = new PictureLabel();
            pictureLabel_Difficulty_Hard_Score = new PictureLabel();
            pictureLabel_Difficulty_Normal = new PictureLabel();
            pictureLabel_Difficulty_Normal_Score = new PictureLabel();
            pictureLabel_Difficulty_Easy = new PictureLabel();
            pictureLabel_Difficulty_Easy_Score = new PictureLabel();
            pictureLabel_Speed = new PictureLabel();

            Controls.Add(pictureLabel_Music_Name);
            Controls.Add(pictureLabel_Music_Information_Label);
            Controls.Add(pictureLabel_Music_Information);
            Controls.Add(pictureLabel_Difficulty_Hard);
            Controls.Add(pictureLabel_Difficulty_Hard_Score);
            Controls.Add(pictureLabel_Difficulty_Normal);
            Controls.Add(pictureLabel_Difficulty_Normal_Score);
            Controls.Add(pictureLabel_Difficulty_Easy);
            Controls.Add(pictureLabel_Difficulty_Easy_Score);
            Controls.Add(pictureLabel_Speed);

            pictureLabel_Music_Name.BringToFront();
            pictureLabel_Music_Information_Label.BringToFront();
            pictureLabel_Music_Information.BringToFront();
            pictureLabel_Difficulty_Hard.BringToFront();
            pictureLabel_Difficulty_Hard_Score.BringToFront();
            pictureLabel_Difficulty_Normal.BringToFront();
            pictureLabel_Difficulty_Normal_Score.BringToFront();
            pictureLabel_Difficulty_Easy.BringToFront();
            pictureLabel_Difficulty_Easy_Score.BringToFront();
            pictureLabel_Speed.BringToFront();

            CopyLabel(pictureLabel_Music_Name, label_Music_Name);
            CopyLabel(pictureLabel_Music_Information_Label, label_Music_Information_Label);
            CopyLabel(pictureLabel_Music_Information, label_Music_Information);
            CopyLabel(pictureLabel_Difficulty_Hard, label_Difficulty_Hard);
            CopyLabel(pictureLabel_Difficulty_Hard_Score, label_Difficulty_Hard_Score);
            CopyLabel(pictureLabel_Difficulty_Normal, label_Difficulty_Normal);
            CopyLabel(pictureLabel_Difficulty_Normal_Score, label_Difficulty_Normal_Score);
            CopyLabel(pictureLabel_Difficulty_Easy, label_Difficulty_Easy);
            CopyLabel(pictureLabel_Difficulty_Easy_Score, label_Difficulty_Easy_Score);
            CopyLabel(pictureLabel_Speed, label_Speed);

            pictureLabel_Music_Name.Font = font_musicName;
            pictureLabel_Music_Information.Font = font_musicInformation;
            pictureLabel_Music_Information_Label.Font = font_musicInformation;

            pictureLabel_Music_Information.Left = pictureLabel_Music_Information_Label.Left +
                                                  pictureLabel_Music_Information_Label.Width;
            pictureLabel_Music_Information.Top = pictureLabel_Music_Information_Label.Top;

            pictureLabel_Difficulty_Hard.Font = font_difficulty;
            pictureLabel_Difficulty_Hard_Score.Font = font_difficulty;
            pictureLabel_Difficulty_Normal.Font = font_difficulty;
            pictureLabel_Difficulty_Normal_Score.Font = font_difficulty;
            pictureLabel_Difficulty_Easy.Font = font_difficulty;
            pictureLabel_Difficulty_Easy_Score.Font = font_difficulty;

            pictureLabel_Speed.Font = font_speed;

            pictureLabel_Difficulty_Hard.Left -= pictureBox_Difficulty_Hard.Left;
            pictureLabel_Difficulty_Hard.Top -= pictureBox_Difficulty_Hard.Top;
            pictureLabel_Difficulty_Hard_Score.Left -= pictureBox_Difficulty_Hard.Left;
            pictureLabel_Difficulty_Hard_Score.Top -= pictureBox_Difficulty_Hard.Top;
            pictureBox_Difficulty_Hard_Rank.Left -= pictureBox_Difficulty_Hard.Left;
            pictureBox_Difficulty_Hard_Rank.Top -= pictureBox_Difficulty_Hard.Top;
            pictureLabel_Difficulty_Normal.Left = pictureLabel_Difficulty_Hard.Left;
            pictureLabel_Difficulty_Normal.Top = pictureLabel_Difficulty_Hard.Top;
            pictureLabel_Difficulty_Normal_Score.Left = pictureLabel_Difficulty_Hard_Score.Left;
            pictureLabel_Difficulty_Normal_Score.Top = pictureLabel_Difficulty_Hard_Score.Top;
            pictureBox_Difficulty_Normal_Rank.Left = pictureBox_Difficulty_Hard_Rank.Left;
            pictureBox_Difficulty_Normal_Rank.Top = pictureBox_Difficulty_Hard_Rank.Top;
            pictureLabel_Difficulty_Easy.Left = pictureLabel_Difficulty_Hard.Left;
            pictureLabel_Difficulty_Easy.Top = pictureLabel_Difficulty_Hard.Top;
            pictureLabel_Difficulty_Easy_Score.Left = pictureLabel_Difficulty_Hard_Score.Left;
            pictureLabel_Difficulty_Easy_Score.Top = pictureLabel_Difficulty_Hard_Score.Top;
            pictureBox_Difficulty_Easy_Rank.Left = pictureBox_Difficulty_Hard_Rank.Left;
            pictureBox_Difficulty_Easy_Rank.Top = pictureBox_Difficulty_Hard_Rank.Top;

            pictureLabel_Difficulty_Hard.Parent = pictureBox_Difficulty_Hard;
            pictureLabel_Difficulty_Hard_Score.Parent = pictureBox_Difficulty_Hard;
            pictureBox_Difficulty_Hard_Rank.Parent = pictureBox_Difficulty_Hard;
            pictureLabel_Difficulty_Normal.Parent = pictureBox_Difficulty_Normal;
            pictureLabel_Difficulty_Normal_Score.Parent = pictureBox_Difficulty_Normal;
            pictureBox_Difficulty_Normal_Rank.Parent = pictureBox_Difficulty_Normal;
            pictureLabel_Difficulty_Easy.Parent = pictureBox_Difficulty_Easy;
            pictureLabel_Difficulty_Easy_Score.Parent = pictureBox_Difficulty_Easy;
            pictureBox_Difficulty_Easy_Rank.Parent = pictureBox_Difficulty_Easy;

            controls = new List<Control>[4];
            controls[0] = new List<Control>();
            controls[1] = new List<Control>();
            controls[2] = new List<Control>();
            controls[3] = new List<Control>();
            controls[0].Add(pictureBox_AlbumList1);
            controls[0].Add(pictureBox_AlbumList2);
            controls[0].Add(pictureBox_AlbumList3);
            controls[0].Add(pictureBox_AlbumList4);
            controls[0].Add(pictureBox_AlbumList5);
            controls[0].Add(pictureBox_AlbumList6);
            controls[0].Add(pictureBox_AlbumList7);
            controls[0].Add(pictureBox_AlbumList8);
            controls[0].Add(pictureLabel_Music_Name);
            controls[0].Add(pictureBox_Arrow_Left);
            controls[0].Add(pictureBox_Arrow_Right);
            controls[1].Add(pictureBox_Album_Selected);
            controls[1].Add(pictureLabel_Music_Information);
            controls[1].Add(pictureLabel_Music_Information_Label);
            controls[1].Add(pictureBox_Difficulty_Hard);
            controls[1].Add(pictureBox_Difficulty_Hard_Rank);
            controls[1].Add(pictureBox_Difficulty_Normal);
            controls[1].Add(pictureBox_Difficulty_Normal_Rank);
            controls[1].Add(pictureBox_Difficulty_Easy);
            controls[1].Add(pictureBox_Difficulty_Easy_Rank);
            controls[1].Add(pictureBox_Difficulty_Cancel);
            controls[1].Add(pictureLabel_Difficulty_Hard);
            controls[1].Add(pictureLabel_Difficulty_Hard_Score);
            controls[1].Add(pictureLabel_Difficulty_Normal);
            controls[1].Add(pictureLabel_Difficulty_Normal_Score);
            controls[1].Add(pictureLabel_Difficulty_Easy);
            controls[1].Add(pictureLabel_Difficulty_Easy_Score);
            controls[2].Add(pictureBox_Difficulty_Hard);
            controls[2].Add(pictureBox_Difficulty_Hard_Rank);
            controls[2].Add(pictureBox_Difficulty_Normal);
            controls[2].Add(pictureBox_Difficulty_Normal_Rank);
            controls[2].Add(pictureBox_Difficulty_Easy);
            controls[2].Add(pictureBox_Difficulty_Easy_Rank);
            controls[2].Add(pictureBox_Difficulty_Cancel);
            controls[2].Add(pictureLabel_Difficulty_Hard);
            controls[2].Add(pictureLabel_Difficulty_Hard_Score);
            controls[2].Add(pictureLabel_Difficulty_Normal);
            controls[2].Add(pictureLabel_Difficulty_Normal_Score);
            controls[2].Add(pictureLabel_Difficulty_Easy);
            controls[2].Add(pictureLabel_Difficulty_Easy_Score);
            controls[3].Add(pictureBox_Speed_Title);
            controls[3].Add(pictureLabel_Speed);

            playState = 0;

            ChangePage();
            ChangeMusic(true);
            selectedMusicTick = 0;
            LoadMusic();

            SetControlsVisible(0, true);
            SetControlsVisible(1, false);
            SetControlsVisible(2, false);
            
            timer_Main.Enabled = true;
        }
        ~Form_Main()
        {
            if (sound_music != null)
            {
                sound_music.release();
            }

            if (sound_ok != null)
            {
                sound_ok.release();
            }

            if (sound_select != null)
            {
                sound_select.release();
            }

            if (sound_cancel != null)
            {
                sound_cancel.release();
            }

            if (system != null)
            {
                system.close();
                system.release();
            }
        }

        private List<String> GetFileList(String path)
        {
            DirectoryInfo directoryInfo = new DirectoryInfo(path);

            DirectoryInfo[] directoryInfos = directoryInfo.GetDirectories();
            FileInfo[] fileInfos = directoryInfo.GetFiles();

            List<String> fileList = new List<String>();

            foreach (DirectoryInfo _directoryInfo in directoryInfos)
            {
                fileList.Add(_directoryInfo.Name);
            }

            foreach (FileInfo fileInfo in fileInfos)
            {
                fileList.Add(fileInfo.Name);
            }

            return fileList;
        }

        private void timer_Serial_Tick(object sender, EventArgs e)
        {
            if (playState == 0 && serialPort_Main.IsOpen)
            {
                try
                {
                    datas += serialPort_Main.ReadExisting();

                    int lastIndex2 = datas.LastIndexOf('|');

                    if (lastIndex2 != -1)
                    {
                        int lastIndex = datas.LastIndexOf('!', lastIndex2 - 1);

                        if (lastIndex != -1 && lastIndex < lastIndex2)
                        {
                            String currentData = datas.Substring(lastIndex + 1, lastIndex2 - (lastIndex + 1));

                            datas = datas.Substring(lastIndex2 + 1);

                            data = new Data(currentData);

                            UpdateData();
                        }
                    }
                }
                catch (Exception /*e*/)
                {
                    datas = "";
                }
            }
            else if (playState == 1)
            {
                if (FindWindow("BlUE Voltex", "BlUE Voltex").ToInt32() != 0)
                {
                    playState = 2;
                }
            }
            else if (playState == 2)
            {
                if (FindWindow("BlUE Voltex", "BlUE Voltex").ToInt32() == 0)
                {
                    ChangeMusic(true);
                    selectedMusicTick = 0;
                    LoadMusic();

                    Show();

                    playState = 0;
                }
            }
            else
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_SerialPort");

                foreach (ManagementObject managementObject in searcher.Get())
                {
                    String caption = managementObject["Caption"].ToString();

                    if (managementObject["Caption"].ToString().Contains("Arduino Mega ADK"))
                    {
                        serialPort_Main.Close();
                        serialPort_Main.PortName = managementObject["DeviceID"].ToString();
                        serialPort_Main.Open();

                        serialPort_Main.ReadExisting();

                        serialPort_Main.Write("!0 0 255 255 0 0 |");
                    }
                }
            }
        }

        private void UpdateData()
        {
            int tick = Environment.TickCount & Int32.MaxValue;

            if (tick > moveTick + 200)
            {
                if (data.joystick[0] < 1023 / 4)
                {
                    if (state == 0)
                    {
                        if (selectedMusicIndex == 0 ||
                            selectedMusicIndex == 4)
                        {
                            if (currentPage > 0)
                            {
                                --currentPage;
                                selectedMusicIndex += 3;
                                ChangePage();
                                ChangeMusic();
                            }
                        }
                        else
                        {
                            --selectedMusicIndex;
                            ChangeMusic();
                        }
                    }
                    else if (state == 2)
                    {
                        if (speed > 5)
                        {
                            --speed;
                            ChangeSpeed();
                        }
                    }
                }
                else if (data.joystick[0] > 1023 * 3 / 4)
                {
                    if (state == 0)
                    {
                        if (selectedMusicIndex == 3)
                        {
                            if (musicList.Count - 1 > (currentPage * 8) + selectedMusicIndex + 4)
                            {
                                ++currentPage;
                                selectedMusicIndex -= 3;
                                ChangePage();
                                ChangeMusic();
                            }
                        }
                        else if (selectedMusicIndex == 7)
                        {
                            if (musicList.Count - 1 > (currentPage * 8) + selectedMusicIndex + 4)
                            {
                                ++currentPage;
                                selectedMusicIndex -= 3;
                                ChangePage();
                                ChangeMusic();
                            }
                            else if (musicList.Count - 1 > (currentPage * 8) + selectedMusicIndex)
                            {
                                ++currentPage;
                                selectedMusicIndex = 0;
                                ChangePage();
                                ChangeMusic();
                            }
                        }
                        else
                        {
                            if (musicList.Count - 1 > (currentPage * 8) + selectedMusicIndex)
                            {
                                ++selectedMusicIndex;
                                ChangeMusic();
                            }
                        }
                    }
                    else if (state == 2)
                    {
                        if (speed < 30)
                        {
                            ++speed;
                            ChangeSpeed();
                        }
                    }
                }

                if (data.joystick[1] < 1023 / 4)
                {
                    if (state == 0)
                    {
                        if (musicList.Count - 1 > (currentPage * 8) + selectedMusicIndex + 3)
                        {
                            if (selectedMusicIndex < 4)
                            {
                                selectedMusicIndex += 4;
                                ChangeMusic();
                            }
                        }
                    }
                    else if (state == 1)
                    {
                        selectedDifficultyIndex = ++selectedDifficultyIndex % 4;

                        ChangeDifficulty();
                    }
                }
                else if (data.joystick[1] > 1023 * 3 / 4)
                {
                    if (state == 0)
                    {
                        if (selectedMusicIndex > 3)
                        {
                            selectedMusicIndex -= 4;
                            ChangeMusic();
                        }
                    }
                    else if (state == 1)
                    {
                        selectedDifficultyIndex = (selectedDifficultyIndex + 3) % 4;

                        ChangeDifficulty();
                    }
                }

                moveTick = tick;
            }

            if (data.joystick[0] > 1023 / 4 && data.joystick[0] < 1023 * 3 / 4 &&
                data.joystick[1] > 1023 / 4 && data.joystick[1] < 1023 * 3 / 4)
            {
                moveTick = 0;
            }

            if (data.joystick[2] == 1)
            {
                if (!isClicked)
                {
                    bool check = true;

                    if (state == 0)
                    {
                        if (selectedMusicTick == 0)
                        {
                            selectedDifficultyIndex = 0;
                            ChangeDifficulty();

                            SetControlsVisible(0, false);
                            SetControlsVisible(1, true);

                            ++state;
                        }
                        else
                        {
                            check = false;
                        }
                    }
                    else if (state == 1)
                    {
                        if (selectedDifficultyIndex < 3)
                        {
                            SetControlsVisible(2, false);
                            SetControlsVisible(3, true);

                            ++state;
                        }
                        else
                        {
                            SetControlsVisible(1, false);
                            SetControlsVisible(0, true);

                            --state;

                            system.playSound(sound_cancel, null, false, out channel_cancel);
                            check = false;
                        }
                    }
                    else if (state == 2 && playState == 0)
                    {
                        String parameter = serialPort_Main.PortName +
                                           " " + musicList[(currentPage * 8) + selectedMusicIndex] +
                                           " " + (2 - selectedDifficultyIndex).ToString() +
                                           " " + pictureLabel_Speed.Text;

                        channel_music.stop();

                        serialPort_Main.Close();
                        datas = "";

                        Process.Start("BlUE Voltex Play.exe", parameter);

                        Hide();

                        SetControlsVisible(3, false);
                        SetControlsVisible(2, true);

                        --state;

                        playState = 1;
                     }

                    if (check)
                    {
                        system.playSound(sound_ok, null, false, out channel_ok);
                    }

                    isClicked = true;
                }
            }
            else
            {
                isClicked = false;
            }

            if (data.cancel == 1)
            {
                if (!isCancelled)
                {
                    if (state == 1)
                    {
                        SetControlsVisible(1, false);
                        SetControlsVisible(0, true);

                        pictureBox_Difficulty_Hard.Image = Properties.Resources.difficulty_hard_selected;
                        pictureBox_Difficulty_Normal.Image = Properties.Resources.difficulty_normal;
                        pictureBox_Difficulty_Easy.Image = Properties.Resources.difficulty_easy;

                        --state;
                    }
                    else if (state == 2)
                    {
                        SetControlsVisible(3, false);
                        SetControlsVisible(2, true);

                        --state;
                    }

                    system.playSound(sound_cancel, null, false, out channel_cancel);

                    isCancelled = true;
                }
            }
            else
            {
                isCancelled = false;
            }
        }

        private void ChangePage()
        {
            if (currentPage > 0)
            {
                pictureBox_Arrow_Left.Image = Properties.Resources.arrow_left;
            }
            else if (pictureBox_Arrow_Left.Image != null)
            {
                pictureBox_Arrow_Left.Image.Dispose();
                pictureBox_Arrow_Left.Image = null;
            }

            int count = musicList.Count - (currentPage * 8);

            if (count > 8)
            {
                pictureBox_Arrow_Right.Image = Properties.Resources.arrow_right;

                count = 8;
            }
            else if (pictureBox_Arrow_Right.Image != null)
            {
                pictureBox_Arrow_Right.Image.Dispose();
                pictureBox_Arrow_Right.Image = null;
            }

            for (int i = 0; i < 8; ++i)
            {
                if (i < count)
                {
                    int index = (currentPage * 8) + i;

                    Graphics graphics = Graphics.FromImage(albumSmallBitmap);
                    graphics.Clear(Color.Transparent);
                    graphics.DrawImage(Image.FromFile("music/" + musicList[index] + "/" + Music.FILE_IMAGE),
                                                        24, 24, 124, 124);

                    if (albumList[i].BackgroundImage != null)
                    {
                        albumList[i].BackgroundImage.Dispose();
                    }

                    albumList[i].BackgroundImage = (Image)albumSmallBitmap.Clone();
                }
                else
                {
                    if (albumList[i].BackgroundImage != null)
                    {
                        albumList[i].BackgroundImage.Dispose();
                        albumList[i].BackgroundImage = null;
                    }

                    if (albumList[i].Image != null)
                    {
                        albumList[i].Image.Dispose();
                        albumList[i].Image = null;
                    }
                }
            }
        }

        private void ChangeMusic(bool muteSound = false)
        {
            int count = musicList.Count - (currentPage * 8);

            if (count > 8)
            {
                count = 8;
            }

            for (int i = 0; i < count; ++i)
            {
                if (i == selectedMusicIndex)
                {
                    Graphics.FromImage(albumList[i].BackgroundImage).DrawImage(
                        Properties.Resources.album_small_selected,
                        0, 0, albumList[i].Width, albumList[i].Height);
                }
                else
                {
                    albumList[i].Image = Properties.Resources.album_small;
                }
            }

            if (!muteSound)
            {
                system.playSound(sound_select, null, false, out channel_select);
            }

            if (channel_music != null)
            {
                channel_music.stop();
            }

            music.Load("music/" + musicList[(currentPage * 8) + selectedMusicIndex], true);

            pictureLabel_Music_Name.Text = music.information.name;

            selectedMusicTick = Environment.TickCount & Int32.MaxValue;
        }

        private void LoadMusic()
        {
            Graphics graphics = Graphics.FromImage(albumBigBitmap);
            graphics.Clear(Color.Transparent);
            graphics.DrawImage(
                Image.FromFile("music/" + musicList[(currentPage * 8) + selectedMusicIndex] + "/" + Music.FILE_IMAGE),
                21, 20, 238, 238);
            graphics.DrawImage(Properties.Resources.album_big, 0, 0,
                               pictureBox_Album_Selected.Width, pictureBox_Album_Selected.Height);

            if (pictureBox_Album_Selected.BackgroundImage != null)
            {
                pictureBox_Album_Selected.BackgroundImage.Dispose();
            }

            pictureBox_Album_Selected.BackgroundImage = (Image)albumBigBitmap.Clone();

            pictureLabel_Music_Information.Text = ": " + music.information.name + "\n" +
                                                  ": " + music.information.artist + "\n" +
                                                  ": " + music.information.startBPM.ToString();

            float score = 0.0f;

            pictureLabel_Difficulty_Hard.Text = music.information.hardDifficulty.ToString();
            float.TryParse(
                music.ReadText("music/" + musicList[(currentPage * 8) + selectedMusicIndex] + "/score_hard.dat"),
                out score);
            pictureLabel_Difficulty_Hard_Score.Text = ((int)score).ToString() + "." +
                                                      ((int)(score * 10.0f) % 10).ToString();

            if (score >= 95.0f)
            {
                pictureBox_Difficulty_Hard_Rank.Image = Properties.Resources.rank_ss;
            }
            else if (score >= 90.0f)
            {
                pictureBox_Difficulty_Hard_Rank.Image = Properties.Resources.rank_s;
            }
            else if (score >= 80.0f)
            {
                pictureBox_Difficulty_Hard_Rank.Image = Properties.Resources.rank_a;
            }
            else if (score >= 70.0f)
            {
                pictureBox_Difficulty_Hard_Rank.Image = Properties.Resources.rank_b;
            }
            else if (score <= 0.0f)
            {
                pictureBox_Difficulty_Hard_Rank.Image = Properties.Resources.rank_blank;
            }
            else
            {
                pictureBox_Difficulty_Hard_Rank.Image = Properties.Resources.rank_c;
            }

            score = 0.0f;

            pictureLabel_Difficulty_Normal.Text = music.information.normalDifficulty.ToString();
            float.TryParse(
                music.ReadText("music/" + musicList[(currentPage * 8) + selectedMusicIndex] + "/score_normal.dat"),
                out score);
            pictureLabel_Difficulty_Normal_Score.Text = ((int)score).ToString() + "." +
                                                        ((int)(score * 10.0f) % 10).ToString();

            if (score >= 95.0f)
            {
                pictureBox_Difficulty_Normal_Rank.Image = Properties.Resources.rank_ss;
            }
            else if (score >= 90.0f)
            {
                pictureBox_Difficulty_Normal_Rank.Image = Properties.Resources.rank_s;
            }
            else if (score >= 80.0f)
            {
                pictureBox_Difficulty_Normal_Rank.Image = Properties.Resources.rank_a;
            }
            else if (score >= 70.0f)
            {
                pictureBox_Difficulty_Normal_Rank.Image = Properties.Resources.rank_b;
            }
            else if (score <= 0.0f)
            {
                pictureBox_Difficulty_Normal_Rank.Image = Properties.Resources.rank_blank;
            }
            else
            {
                pictureBox_Difficulty_Normal_Rank.Image = Properties.Resources.rank_c;
            }

            score = 0.0f;

            pictureLabel_Difficulty_Easy.Text = music.information.easyDifficulty.ToString();
            float.TryParse(
                music.ReadText("music/" + musicList[(currentPage * 8) + selectedMusicIndex] + "/score_easy.dat"),
                out score);
            pictureLabel_Difficulty_Easy_Score.Text = ((int)score).ToString() + "." +
                                                      ((int)(score * 10.0f) % 10).ToString();

            if (score >= 95.0f)
            {
                pictureBox_Difficulty_Easy_Rank.Image = Properties.Resources.rank_ss;
            }
            else if (score >= 90.0f)
            {
                pictureBox_Difficulty_Easy_Rank.Image = Properties.Resources.rank_s;
            }
            else if (score >= 80.0f)
            {
                pictureBox_Difficulty_Easy_Rank.Image = Properties.Resources.rank_a;
            }
            else if (score >= 70.0f)
            {
                pictureBox_Difficulty_Easy_Rank.Image = Properties.Resources.rank_b;
            }
            else if (score <= 0.0f)
            {
                pictureBox_Difficulty_Easy_Rank.Image = Properties.Resources.rank_blank;
            }
            else
            {
                pictureBox_Difficulty_Easy_Rank.Image = Properties.Resources.rank_c;
            }

            system.createSound(music.savePath + Music.FILE_MUSIC, MODE.DEFAULT, out sound_music);
            system.playSound(sound_music, null, true, out channel_music);
            channel_music.setVolume(0.0f);
            channel_music.setPosition((uint)music.information.previewStartTime, TIMEUNIT.MS);
            channel_music.setPaused(false);
        }

        private void ChangeDifficulty()
        {
            for (int i = 0; i < 4; ++i)
            {
                if (i == selectedDifficultyIndex)
                {
                    if (i == 0)
                    {
                        difficultyList[i].Image = Properties.Resources.difficulty_hard_selected;
                    }
                    else if (i == 1)
                    {
                        difficultyList[i].Image = Properties.Resources.difficulty_normal_selected;
                    }
                    else if (i == 2)
                    {
                        difficultyList[i].Image = Properties.Resources.difficulty_easy_selected;
                    }
                    else if (i == 3)
                    {
                        difficultyList[i].Image = Properties.Resources.difficulty_cancel_selected;
                    }
                }
                else
                {
                    if (i == 0)
                    {
                        difficultyList[i].Image = Properties.Resources.difficulty_hard;
                    }
                    else if (i == 1)
                    {
                        difficultyList[i].Image = Properties.Resources.difficulty_normal;
                    }
                    else if (i == 2)
                    {
                        difficultyList[i].Image = Properties.Resources.difficulty_easy;
                    }
                    else if (i == 3)
                    {
                        difficultyList[i].Image = Properties.Resources.difficulty_cancel;
                    }
                }
            }

            system.playSound(sound_select, null, false, out channel_select);
        }

        private void ChangeSpeed()
        {
            pictureLabel_Speed.Text = (speed / 10).ToString() + "." +
                                      (speed % 10).ToString();

            system.playSound(sound_select, null, false, out channel_select);
        }

        private void timer_Main_Tick(object sender, EventArgs e)
        {
            if (playState != 0)
            {
                return;
            }

            system.update();

            uint musicPosition;
            channel_music.getPosition(out musicPosition, TIMEUNIT.MS);

            long previewPosition = musicPosition - music.information.previewStartTime;

            if (previewPosition > music.information.previewPlayTime || musicPosition == 0)
            {
                channel_music.setPosition((uint)music.information.previewStartTime, TIMEUNIT.MS);
            }

            float volumeRate = 1.0f;

            if (previewPosition < 1000L)
            {
                volumeRate = previewPosition / 1000.0f;
            }
            else if (previewPosition > music.information.previewPlayTime - 1000)
            {
                volumeRate = (music.information.previewPlayTime - previewPosition) / 1000.0f;
            }

            channel_music.setVolume(music.information.musicVolume * volumeRate);

            int tick = Environment.TickCount & Int32.MaxValue;
            float lightRate = ((tick % 1000) - 500) / 1000.0f;
            lightRate = Math.Abs(lightRate);

            if (albumList[selectedMusicIndex].Image != null)
            {
                albumList[selectedMusicIndex].Image.Dispose();
            }

            albumList[selectedMusicIndex].Image = (Image)albumList[selectedMusicIndex].BackgroundImage.Clone();

            Graphics.FromImage(albumList[selectedMusicIndex].Image).DrawImage(
                ChangeOpacity(Properties.Resources.album_small_selected_light, lightRate),
                0, 0, albumList[selectedMusicIndex].Width, albumList[selectedMusicIndex].Height);

            if (pictureBox_Album_Selected.Image != null)
            {
                pictureBox_Album_Selected.Image.Dispose();
            }

            pictureBox_Album_Selected.Image = (Image)pictureBox_Album_Selected.BackgroundImage.Clone();

            Graphics.FromImage(pictureBox_Album_Selected.Image).DrawImage(
                ChangeOpacity(Properties.Resources.album_big_light, lightRate),
                0, 0, pictureBox_Album_Selected.Width, pictureBox_Album_Selected.Height);

            if (selectedMusicTick > 0 && tick > selectedMusicTick + 500)
            {
                LoadMusic();

                selectedMusicTick = 0;
            }
        }

        private Bitmap ChangeOpacity(Image img, float opacityvalue)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height);
            Graphics graphics = Graphics.FromImage(bmp);
            ColorMatrix colormatrix = new ColorMatrix();
            colormatrix.Matrix33 = opacityvalue;
            ImageAttributes imgAttribute = new ImageAttributes();
            imgAttribute.SetColorMatrix(colormatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);
            graphics.DrawImage(img, new Rectangle(0, 0, bmp.Width, bmp.Height), 0, 0, img.Width, img.Height, GraphicsUnit.Pixel, imgAttribute);
            graphics.Dispose(); 
            return bmp;
        }

        private void SetControlsVisible(int index, bool visible)
        {
            foreach (Control control in controls[index])
            {
                control.Visible = visible;
            }
        }

        private void CopyLabel(Label to, Label from)
        {
            to.Bounds = from.Bounds;
            to.BackColor = from.BackColor;
            to.Image = from.Image;
            to.AutoSize = from.AutoSize;
            to.Text = from.Text;
            to.ForeColor = from.ForeColor;
            to.TextAlign = from.TextAlign;
            to.Visible = from.Visible;
        }
    }
}

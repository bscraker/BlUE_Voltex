﻿namespace BlUE_Voltex
{
    partial class Form_Main
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.pictureBox_AlbumList1 = new System.Windows.Forms.PictureBox();
            this.pictureBox_AlbumList2 = new System.Windows.Forms.PictureBox();
            this.pictureBox_AlbumList4 = new System.Windows.Forms.PictureBox();
            this.pictureBox_AlbumList3 = new System.Windows.Forms.PictureBox();
            this.pictureBox_AlbumList8 = new System.Windows.Forms.PictureBox();
            this.pictureBox_AlbumList7 = new System.Windows.Forms.PictureBox();
            this.pictureBox_AlbumList6 = new System.Windows.Forms.PictureBox();
            this.pictureBox_AlbumList5 = new System.Windows.Forms.PictureBox();
            this.pictureBox_Arrow_Left = new System.Windows.Forms.PictureBox();
            this.pictureBox_Arrow_Right = new System.Windows.Forms.PictureBox();
            this.serialPort_Main = new System.IO.Ports.SerialPort(this.components);
            this.timer_Serial = new System.Windows.Forms.Timer(this.components);
            this.timer_Main = new System.Windows.Forms.Timer(this.components);
            this.label_Music_Name = new System.Windows.Forms.Label();
            this.label_Music_Information = new System.Windows.Forms.Label();
            this.label_Music_Information_Label = new System.Windows.Forms.Label();
            this.pictureBox_Album_Selected = new System.Windows.Forms.PictureBox();
            this.pictureBox_Difficulty_Hard = new System.Windows.Forms.PictureBox();
            this.pictureBox_Difficulty_Normal = new System.Windows.Forms.PictureBox();
            this.pictureBox_Difficulty_Easy = new System.Windows.Forms.PictureBox();
            this.pictureBox_Difficulty_Cancel = new System.Windows.Forms.PictureBox();
            this.label_Difficulty_Hard_Score = new System.Windows.Forms.Label();
            this.pictureBox_Difficulty_Hard_Rank = new System.Windows.Forms.PictureBox();
            this.pictureBox_Difficulty_Easy_Rank = new System.Windows.Forms.PictureBox();
            this.label_Difficulty_Easy_Score = new System.Windows.Forms.Label();
            this.label_Difficulty_Easy = new System.Windows.Forms.Label();
            this.label_Difficulty_Hard = new System.Windows.Forms.Label();
            this.label_Difficulty_Normal = new System.Windows.Forms.Label();
            this.label_Difficulty_Normal_Score = new System.Windows.Forms.Label();
            this.pictureBox_Difficulty_Normal_Rank = new System.Windows.Forms.PictureBox();
            this.label_Speed = new System.Windows.Forms.Label();
            this.pictureBox_Speed_Title = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Arrow_Left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Arrow_Right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Album_Selected)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Hard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Normal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Easy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Cancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Hard_Rank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Easy_Rank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Normal_Rank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Speed_Title)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox_AlbumList1
            // 
            this.pictureBox_AlbumList1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_AlbumList1.Location = new System.Drawing.Point(55, 185);
            this.pictureBox_AlbumList1.Name = "pictureBox_AlbumList1";
            this.pictureBox_AlbumList1.Size = new System.Drawing.Size(168, 168);
            this.pictureBox_AlbumList1.TabIndex = 0;
            this.pictureBox_AlbumList1.TabStop = false;
            this.pictureBox_AlbumList1.Visible = false;
            // 
            // pictureBox_AlbumList2
            // 
            this.pictureBox_AlbumList2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_AlbumList2.Location = new System.Drawing.Point(229, 185);
            this.pictureBox_AlbumList2.Name = "pictureBox_AlbumList2";
            this.pictureBox_AlbumList2.Size = new System.Drawing.Size(168, 168);
            this.pictureBox_AlbumList2.TabIndex = 1;
            this.pictureBox_AlbumList2.TabStop = false;
            this.pictureBox_AlbumList2.Visible = false;
            // 
            // pictureBox_AlbumList4
            // 
            this.pictureBox_AlbumList4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_AlbumList4.Location = new System.Drawing.Point(577, 185);
            this.pictureBox_AlbumList4.Name = "pictureBox_AlbumList4";
            this.pictureBox_AlbumList4.Size = new System.Drawing.Size(168, 168);
            this.pictureBox_AlbumList4.TabIndex = 3;
            this.pictureBox_AlbumList4.TabStop = false;
            this.pictureBox_AlbumList4.Visible = false;
            // 
            // pictureBox_AlbumList3
            // 
            this.pictureBox_AlbumList3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_AlbumList3.Location = new System.Drawing.Point(403, 185);
            this.pictureBox_AlbumList3.Name = "pictureBox_AlbumList3";
            this.pictureBox_AlbumList3.Size = new System.Drawing.Size(168, 168);
            this.pictureBox_AlbumList3.TabIndex = 2;
            this.pictureBox_AlbumList3.TabStop = false;
            this.pictureBox_AlbumList3.Visible = false;
            // 
            // pictureBox_AlbumList8
            // 
            this.pictureBox_AlbumList8.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_AlbumList8.Location = new System.Drawing.Point(577, 410);
            this.pictureBox_AlbumList8.Name = "pictureBox_AlbumList8";
            this.pictureBox_AlbumList8.Size = new System.Drawing.Size(168, 168);
            this.pictureBox_AlbumList8.TabIndex = 7;
            this.pictureBox_AlbumList8.TabStop = false;
            this.pictureBox_AlbumList8.Visible = false;
            // 
            // pictureBox_AlbumList7
            // 
            this.pictureBox_AlbumList7.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_AlbumList7.Location = new System.Drawing.Point(403, 410);
            this.pictureBox_AlbumList7.Name = "pictureBox_AlbumList7";
            this.pictureBox_AlbumList7.Size = new System.Drawing.Size(168, 168);
            this.pictureBox_AlbumList7.TabIndex = 6;
            this.pictureBox_AlbumList7.TabStop = false;
            this.pictureBox_AlbumList7.Visible = false;
            // 
            // pictureBox_AlbumList6
            // 
            this.pictureBox_AlbumList6.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_AlbumList6.Location = new System.Drawing.Point(229, 410);
            this.pictureBox_AlbumList6.Name = "pictureBox_AlbumList6";
            this.pictureBox_AlbumList6.Size = new System.Drawing.Size(168, 168);
            this.pictureBox_AlbumList6.TabIndex = 5;
            this.pictureBox_AlbumList6.TabStop = false;
            this.pictureBox_AlbumList6.Visible = false;
            // 
            // pictureBox_AlbumList5
            // 
            this.pictureBox_AlbumList5.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_AlbumList5.Location = new System.Drawing.Point(55, 410);
            this.pictureBox_AlbumList5.Name = "pictureBox_AlbumList5";
            this.pictureBox_AlbumList5.Size = new System.Drawing.Size(168, 168);
            this.pictureBox_AlbumList5.TabIndex = 4;
            this.pictureBox_AlbumList5.TabStop = false;
            this.pictureBox_AlbumList5.Visible = false;
            // 
            // pictureBox_Arrow_Left
            // 
            this.pictureBox_Arrow_Left.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_Arrow_Left.Location = new System.Drawing.Point(5, 351);
            this.pictureBox_Arrow_Left.Name = "pictureBox_Arrow_Left";
            this.pictureBox_Arrow_Left.Size = new System.Drawing.Size(53, 61);
            this.pictureBox_Arrow_Left.TabIndex = 8;
            this.pictureBox_Arrow_Left.TabStop = false;
            this.pictureBox_Arrow_Left.Visible = false;
            // 
            // pictureBox_Arrow_Right
            // 
            this.pictureBox_Arrow_Right.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_Arrow_Right.Location = new System.Drawing.Point(742, 351);
            this.pictureBox_Arrow_Right.Name = "pictureBox_Arrow_Right";
            this.pictureBox_Arrow_Right.Size = new System.Drawing.Size(53, 61);
            this.pictureBox_Arrow_Right.TabIndex = 9;
            this.pictureBox_Arrow_Right.TabStop = false;
            this.pictureBox_Arrow_Right.Visible = false;
            // 
            // timer_Serial
            // 
            this.timer_Serial.Enabled = true;
            this.timer_Serial.Interval = 1;
            this.timer_Serial.Tick += new System.EventHandler(this.timer_Serial_Tick);
            // 
            // timer_Main
            // 
            this.timer_Main.Interval = 1;
            this.timer_Main.Tick += new System.EventHandler(this.timer_Main_Tick);
            // 
            // label_Music_Name
            // 
            this.label_Music_Name.BackColor = System.Drawing.Color.Transparent;
            this.label_Music_Name.Image = global::BlUE_Voltex.Properties.Resources.misic_name;
            this.label_Music_Name.Location = new System.Drawing.Point(80, 350);
            this.label_Music_Name.Name = "label_Music_Name";
            this.label_Music_Name.Size = new System.Drawing.Size(640, 63);
            this.label_Music_Name.TabIndex = 11;
            this.label_Music_Name.Text = "MUSIC NAME";
            this.label_Music_Name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_Music_Name.Visible = false;
            // 
            // label_Music_Information
            // 
            this.label_Music_Information.BackColor = System.Drawing.Color.Transparent;
            this.label_Music_Information.ForeColor = System.Drawing.Color.White;
            this.label_Music_Information.Location = new System.Drawing.Point(160, 468);
            this.label_Music_Information.Name = "label_Music_Information";
            this.label_Music_Information.Size = new System.Drawing.Size(400, 120);
            this.label_Music_Information.TabIndex = 14;
            this.label_Music_Information.Text = "MUISC INFORMATION";
            this.label_Music_Information.Visible = false;
            // 
            // label_Music_Information_Label
            // 
            this.label_Music_Information_Label.AutoSize = true;
            this.label_Music_Information_Label.BackColor = System.Drawing.Color.Transparent;
            this.label_Music_Information_Label.ForeColor = System.Drawing.Color.White;
            this.label_Music_Information_Label.Location = new System.Drawing.Point(50, 468);
            this.label_Music_Information_Label.Name = "label_Music_Information_Label";
            this.label_Music_Information_Label.Size = new System.Drawing.Size(33, 36);
            this.label_Music_Information_Label.TabIndex = 13;
            this.label_Music_Information_Label.Text = "Title\r\nArtist\r\nBPM";
            this.label_Music_Information_Label.Visible = false;
            // 
            // pictureBox_Album_Selected
            // 
            this.pictureBox_Album_Selected.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_Album_Selected.Location = new System.Drawing.Point(34, 188);
            this.pictureBox_Album_Selected.Name = "pictureBox_Album_Selected";
            this.pictureBox_Album_Selected.Size = new System.Drawing.Size(280, 280);
            this.pictureBox_Album_Selected.TabIndex = 12;
            this.pictureBox_Album_Selected.TabStop = false;
            this.pictureBox_Album_Selected.Visible = false;
            // 
            // pictureBox_Difficulty_Hard
            // 
            this.pictureBox_Difficulty_Hard.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_Difficulty_Hard.Location = new System.Drawing.Point(315, 190);
            this.pictureBox_Difficulty_Hard.Name = "pictureBox_Difficulty_Hard";
            this.pictureBox_Difficulty_Hard.Size = new System.Drawing.Size(485, 92);
            this.pictureBox_Difficulty_Hard.TabIndex = 15;
            this.pictureBox_Difficulty_Hard.TabStop = false;
            this.pictureBox_Difficulty_Hard.Visible = false;
            // 
            // pictureBox_Difficulty_Normal
            // 
            this.pictureBox_Difficulty_Normal.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_Difficulty_Normal.Location = new System.Drawing.Point(315, 282);
            this.pictureBox_Difficulty_Normal.Name = "pictureBox_Difficulty_Normal";
            this.pictureBox_Difficulty_Normal.Size = new System.Drawing.Size(485, 92);
            this.pictureBox_Difficulty_Normal.TabIndex = 16;
            this.pictureBox_Difficulty_Normal.TabStop = false;
            this.pictureBox_Difficulty_Normal.Visible = false;
            // 
            // pictureBox_Difficulty_Easy
            // 
            this.pictureBox_Difficulty_Easy.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_Difficulty_Easy.Location = new System.Drawing.Point(315, 374);
            this.pictureBox_Difficulty_Easy.Name = "pictureBox_Difficulty_Easy";
            this.pictureBox_Difficulty_Easy.Size = new System.Drawing.Size(485, 92);
            this.pictureBox_Difficulty_Easy.TabIndex = 17;
            this.pictureBox_Difficulty_Easy.TabStop = false;
            this.pictureBox_Difficulty_Easy.Visible = false;
            // 
            // pictureBox_Difficulty_Cancel
            // 
            this.pictureBox_Difficulty_Cancel.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_Difficulty_Cancel.Location = new System.Drawing.Point(547, 466);
            this.pictureBox_Difficulty_Cancel.Name = "pictureBox_Difficulty_Cancel";
            this.pictureBox_Difficulty_Cancel.Size = new System.Drawing.Size(253, 87);
            this.pictureBox_Difficulty_Cancel.TabIndex = 18;
            this.pictureBox_Difficulty_Cancel.TabStop = false;
            this.pictureBox_Difficulty_Cancel.Visible = false;
            // 
            // label_Difficulty_Hard_Score
            // 
            this.label_Difficulty_Hard_Score.BackColor = System.Drawing.Color.Transparent;
            this.label_Difficulty_Hard_Score.ForeColor = System.Drawing.Color.White;
            this.label_Difficulty_Hard_Score.Location = new System.Drawing.Point(543, 216);
            this.label_Difficulty_Hard_Score.Name = "label_Difficulty_Hard_Score";
            this.label_Difficulty_Hard_Score.Size = new System.Drawing.Size(100, 40);
            this.label_Difficulty_Hard_Score.TabIndex = 20;
            this.label_Difficulty_Hard_Score.Text = "100.0";
            this.label_Difficulty_Hard_Score.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label_Difficulty_Hard_Score.Visible = false;
            // 
            // pictureBox_Difficulty_Hard_Rank
            // 
            this.pictureBox_Difficulty_Hard_Rank.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_Difficulty_Hard_Rank.Location = new System.Drawing.Point(692, 212);
            this.pictureBox_Difficulty_Hard_Rank.Name = "pictureBox_Difficulty_Hard_Rank";
            this.pictureBox_Difficulty_Hard_Rank.Size = new System.Drawing.Size(42, 50);
            this.pictureBox_Difficulty_Hard_Rank.TabIndex = 21;
            this.pictureBox_Difficulty_Hard_Rank.TabStop = false;
            this.pictureBox_Difficulty_Hard_Rank.Visible = false;
            // 
            // pictureBox_Difficulty_Easy_Rank
            // 
            this.pictureBox_Difficulty_Easy_Rank.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_Difficulty_Easy_Rank.Location = new System.Drawing.Point(692, 397);
            this.pictureBox_Difficulty_Easy_Rank.Name = "pictureBox_Difficulty_Easy_Rank";
            this.pictureBox_Difficulty_Easy_Rank.Size = new System.Drawing.Size(42, 50);
            this.pictureBox_Difficulty_Easy_Rank.TabIndex = 27;
            this.pictureBox_Difficulty_Easy_Rank.TabStop = false;
            this.pictureBox_Difficulty_Easy_Rank.Visible = false;
            // 
            // label_Difficulty_Easy_Score
            // 
            this.label_Difficulty_Easy_Score.BackColor = System.Drawing.Color.Transparent;
            this.label_Difficulty_Easy_Score.ForeColor = System.Drawing.Color.White;
            this.label_Difficulty_Easy_Score.Location = new System.Drawing.Point(543, 403);
            this.label_Difficulty_Easy_Score.Name = "label_Difficulty_Easy_Score";
            this.label_Difficulty_Easy_Score.Size = new System.Drawing.Size(100, 40);
            this.label_Difficulty_Easy_Score.TabIndex = 26;
            this.label_Difficulty_Easy_Score.Text = "100.0 %";
            this.label_Difficulty_Easy_Score.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label_Difficulty_Easy_Score.Visible = false;
            // 
            // label_Difficulty_Easy
            // 
            this.label_Difficulty_Easy.BackColor = System.Drawing.Color.Transparent;
            this.label_Difficulty_Easy.ForeColor = System.Drawing.Color.White;
            this.label_Difficulty_Easy.Location = new System.Drawing.Point(373, 402);
            this.label_Difficulty_Easy.Name = "label_Difficulty_Easy";
            this.label_Difficulty_Easy.Size = new System.Drawing.Size(54, 40);
            this.label_Difficulty_Easy.TabIndex = 25;
            this.label_Difficulty_Easy.Text = "10";
            this.label_Difficulty_Easy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_Difficulty_Easy.Visible = false;
            // 
            // label_Difficulty_Hard
            // 
            this.label_Difficulty_Hard.BackColor = System.Drawing.Color.Transparent;
            this.label_Difficulty_Hard.ForeColor = System.Drawing.Color.White;
            this.label_Difficulty_Hard.Location = new System.Drawing.Point(373, 216);
            this.label_Difficulty_Hard.Name = "label_Difficulty_Hard";
            this.label_Difficulty_Hard.Size = new System.Drawing.Size(54, 40);
            this.label_Difficulty_Hard.TabIndex = 19;
            this.label_Difficulty_Hard.Text = "10";
            this.label_Difficulty_Hard.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_Difficulty_Hard.Visible = false;
            // 
            // label_Difficulty_Normal
            // 
            this.label_Difficulty_Normal.BackColor = System.Drawing.Color.Transparent;
            this.label_Difficulty_Normal.ForeColor = System.Drawing.Color.White;
            this.label_Difficulty_Normal.Location = new System.Drawing.Point(373, 310);
            this.label_Difficulty_Normal.Name = "label_Difficulty_Normal";
            this.label_Difficulty_Normal.Size = new System.Drawing.Size(54, 40);
            this.label_Difficulty_Normal.TabIndex = 22;
            this.label_Difficulty_Normal.Text = "10";
            this.label_Difficulty_Normal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_Difficulty_Normal.Visible = false;
            // 
            // label_Difficulty_Normal_Score
            // 
            this.label_Difficulty_Normal_Score.BackColor = System.Drawing.Color.Transparent;
            this.label_Difficulty_Normal_Score.ForeColor = System.Drawing.Color.White;
            this.label_Difficulty_Normal_Score.Location = new System.Drawing.Point(543, 310);
            this.label_Difficulty_Normal_Score.Name = "label_Difficulty_Normal_Score";
            this.label_Difficulty_Normal_Score.Size = new System.Drawing.Size(100, 40);
            this.label_Difficulty_Normal_Score.TabIndex = 23;
            this.label_Difficulty_Normal_Score.Text = "100.0 %";
            this.label_Difficulty_Normal_Score.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label_Difficulty_Normal_Score.Visible = false;
            // 
            // pictureBox_Difficulty_Normal_Rank
            // 
            this.pictureBox_Difficulty_Normal_Rank.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_Difficulty_Normal_Rank.Location = new System.Drawing.Point(692, 304);
            this.pictureBox_Difficulty_Normal_Rank.Name = "pictureBox_Difficulty_Normal_Rank";
            this.pictureBox_Difficulty_Normal_Rank.Size = new System.Drawing.Size(42, 50);
            this.pictureBox_Difficulty_Normal_Rank.TabIndex = 24;
            this.pictureBox_Difficulty_Normal_Rank.TabStop = false;
            this.pictureBox_Difficulty_Normal_Rank.Visible = false;
            // 
            // label_Speed
            // 
            this.label_Speed.BackColor = System.Drawing.Color.Transparent;
            this.label_Speed.ForeColor = System.Drawing.Color.Black;
            this.label_Speed.Image = global::BlUE_Voltex.Properties.Resources.speed;
            this.label_Speed.Location = new System.Drawing.Point(315, 278);
            this.label_Speed.Name = "label_Speed";
            this.label_Speed.Size = new System.Drawing.Size(485, 100);
            this.label_Speed.TabIndex = 28;
            this.label_Speed.Text = "1.0";
            this.label_Speed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label_Speed.Visible = false;
            // 
            // pictureBox_Speed_Title
            // 
            this.pictureBox_Speed_Title.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox_Speed_Title.Image = global::BlUE_Voltex.Properties.Resources.speed_title;
            this.pictureBox_Speed_Title.Location = new System.Drawing.Point(315, 221);
            this.pictureBox_Speed_Title.Name = "pictureBox_Speed_Title";
            this.pictureBox_Speed_Title.Size = new System.Drawing.Size(485, 57);
            this.pictureBox_Speed_Title.TabIndex = 29;
            this.pictureBox_Speed_Title.TabStop = false;
            this.pictureBox_Speed_Title.Visible = false;
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::BlUE_Voltex.Properties.Resources.background;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(800, 600);
            this.Controls.Add(this.label_Difficulty_Easy_Score);
            this.Controls.Add(this.label_Difficulty_Easy);
            this.Controls.Add(this.label_Difficulty_Normal_Score);
            this.Controls.Add(this.label_Difficulty_Normal);
            this.Controls.Add(this.label_Difficulty_Hard_Score);
            this.Controls.Add(this.label_Difficulty_Hard);
            this.Controls.Add(this.pictureBox_Difficulty_Easy_Rank);
            this.Controls.Add(this.pictureBox_Difficulty_Normal_Rank);
            this.Controls.Add(this.pictureBox_Difficulty_Hard_Rank);
            this.Controls.Add(this.pictureBox_Difficulty_Cancel);
            this.Controls.Add(this.pictureBox_Difficulty_Easy);
            this.Controls.Add(this.pictureBox_Difficulty_Normal);
            this.Controls.Add(this.pictureBox_Difficulty_Hard);
            this.Controls.Add(this.pictureBox_Album_Selected);
            this.Controls.Add(this.label_Music_Information);
            this.Controls.Add(this.label_Music_Information_Label);
            this.Controls.Add(this.label_Music_Name);
            this.Controls.Add(this.pictureBox_Arrow_Right);
            this.Controls.Add(this.pictureBox_Arrow_Left);
            this.Controls.Add(this.pictureBox_AlbumList8);
            this.Controls.Add(this.pictureBox_AlbumList7);
            this.Controls.Add(this.pictureBox_AlbumList6);
            this.Controls.Add(this.pictureBox_AlbumList5);
            this.Controls.Add(this.pictureBox_AlbumList4);
            this.Controls.Add(this.pictureBox_AlbumList3);
            this.Controls.Add(this.pictureBox_AlbumList2);
            this.Controls.Add(this.pictureBox_AlbumList1);
            this.Controls.Add(this.pictureBox_Speed_Title);
            this.Controls.Add(this.label_Speed);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form_Main";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "BlUE Voltex";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_AlbumList5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Arrow_Left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Arrow_Right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Album_Selected)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Hard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Normal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Easy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Cancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Hard_Rank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Easy_Rank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Difficulty_Normal_Rank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Speed_Title)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox_AlbumList1;
        private System.Windows.Forms.PictureBox pictureBox_AlbumList2;
        private System.Windows.Forms.PictureBox pictureBox_AlbumList4;
        private System.Windows.Forms.PictureBox pictureBox_AlbumList3;
        private System.Windows.Forms.PictureBox pictureBox_AlbumList8;
        private System.Windows.Forms.PictureBox pictureBox_AlbumList7;
        private System.Windows.Forms.PictureBox pictureBox_AlbumList6;
        private System.Windows.Forms.PictureBox pictureBox_AlbumList5;
        private System.Windows.Forms.PictureBox pictureBox_Arrow_Left;
        private System.Windows.Forms.PictureBox pictureBox_Arrow_Right;
        private System.IO.Ports.SerialPort serialPort_Main;
        private System.Windows.Forms.Timer timer_Serial;
        private System.Windows.Forms.Timer timer_Main;
        private System.Windows.Forms.Label label_Music_Name;
        private System.Windows.Forms.Label label_Music_Information;
        private System.Windows.Forms.Label label_Music_Information_Label;
        private System.Windows.Forms.PictureBox pictureBox_Album_Selected;
        private System.Windows.Forms.PictureBox pictureBox_Difficulty_Hard;
        private System.Windows.Forms.PictureBox pictureBox_Difficulty_Normal;
        private System.Windows.Forms.PictureBox pictureBox_Difficulty_Easy;
        private System.Windows.Forms.PictureBox pictureBox_Difficulty_Cancel;
        private System.Windows.Forms.Label label_Difficulty_Hard_Score;
        private System.Windows.Forms.PictureBox pictureBox_Difficulty_Hard_Rank;
        private System.Windows.Forms.PictureBox pictureBox_Difficulty_Easy_Rank;
        private System.Windows.Forms.Label label_Difficulty_Easy_Score;
        private System.Windows.Forms.Label label_Difficulty_Easy;
        private System.Windows.Forms.Label label_Difficulty_Hard;
        private System.Windows.Forms.Label label_Difficulty_Normal;
        private System.Windows.Forms.Label label_Difficulty_Normal_Score;
        private System.Windows.Forms.PictureBox pictureBox_Difficulty_Normal_Rank;
        private System.Windows.Forms.Label label_Speed;
        private System.Windows.Forms.PictureBox pictureBox_Speed_Title;

    }
}

